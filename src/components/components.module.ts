import { NgModule } from '@angular/core';
import { ProductItemproductComponent } from './product-itemproduct/product-itemproduct';
@NgModule({
	declarations: [ProductItemproductComponent],
	imports: [],
	exports: [ProductItemproductComponent]
})
export class ComponentsModule {}

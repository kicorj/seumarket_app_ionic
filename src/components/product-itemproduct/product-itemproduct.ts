import { Component, Input } from '@angular/core';
import { Product } from '../../shared/models/product.interface';

/**
 * Generated class for the ProductItemproductComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'product-itemproduct',
  templateUrl: 'product-itemproduct.html'
})
export class ProductItemproductComponent {

  @Input()
  product: Product

  constructor() {
    console.log('Hello ProductItemproductComponent Component');
    
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddressPage } from './address';
import {NgxMaskIonicModule} from 'ngx-mask-ionic'

@NgModule({
  declarations: [
    AddressPage,
  ],
  imports: [
    IonicPageModule.forChild(AddressPage),
    NgxMaskIonicModule,
  ],
})
export class AddressPageModule {}

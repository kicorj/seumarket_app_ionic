import { DeliveryDatePage } from './../delivery-date/delivery-date';
import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { Order } from '../../shared/models/order.interface';

@Component({
  selector: "page-address",
  templateUrl: "address.html"
})
export class AddressPage {

  responseData :any;
  orderdataSet:any;
  deliveryDateData:any;
  neighborhoodsData:any;

  addressData: {
    zipcode: string,
    street: string,
    neighborhood: string,
    city: string,
    state: string,
  }

  public userDetails:any;

  postData = {
    token: "",
    user_id:""
  };

  order = {} as Order;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public auth: HkApiproviderProvider,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController
  ) {

    const data = JSON.parse(localStorage.getItem('userData'));
    this.postData.user_id = data.userData.user_id;
    this.postData.token = data.userData.token;
    
    this.order.user_id = data.userData.user_id; 
    this.order.token = data.userData.token;
    
    
    this.getneighborhoods();
    this.getuserdata();

  }

  getuserdata() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    this.auth.postData(this.postData, "getuserdata").then(
      result => {
        this.responseData = result;
        const userData = this.responseData.userData;
        this.order.fname = userData.fname;        
        this.order.mobile = userData.mobile;
        this.order.portaria = userData.portaria;
        this.order.neighborhood_id = userData.neighborhood_id;

        localStorage.setItem('userData', JSON.stringify(this.responseData) );
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  getAdressData() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    let postDataZipcode = {
      zipcode: this.order.zipcode
    };

    this.auth.postData(postDataZipcode, "getaddressbyzipcode").then(
      result => {
        this.responseData = result;
        if (this.responseData) {
          this.addressData = this.responseData;
          this.order.street = this.addressData.street;
          this.order.neighborhood = this.addressData.neighborhood;
          this.order.city = this.addressData.city;
          this.order.state = this.addressData.state;
        }
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  getneighborhoods() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    this.auth.postData(this.postData, "getneighborhoods").then(
      result => {
        this.responseData = result;
        this.neighborhoodsData = this.responseData.NeighborhoodsData;
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  checkLobby() {
    return this.order.portaria === "1";
  }

  stateChangeLobby(event) {
    this.order.portaria = event.value === true ? "1" : "0";
  }

  helpLobby() {
    let alert = this.alertCtrl.create({
      title: 'Deixar na portaria',
      subTitle: '<br/>Indique se você prefere que nosso entregador deixe as compras na portaria do seu prédio ou condomínio.',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit() {

    if(this.order.address && this.order.zipcode && this.order.neighborhood && this.order.street && this.order.street_number && this.order.mobile) {

      const isValidNaighborhood = this.order.city.toLowerCase() == 'rio de janeiro' && this.neighborhoodsData.find(x => x.name === this.order.neighborhood);
      if (!isValidNaighborhood) {
        let alert = this.alertCtrl.create( {
          title: 'Aviso',
          subTitle: 'Que pena, ainda não atendemos nessa localidade. Favor conferir os bairros que fazem parte da nossa Área de Cobertura.',
          buttons: ['OK']
        });
        alert.present();
        return;
      }

      this.navCtrl.push(DeliveryDatePage, {'orderData': this.order});
    } else {
      let alert = this.alertCtrl.create( {
        title: 'Aviso',
        subTitle: 'Por favor preencha todos os campos',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}
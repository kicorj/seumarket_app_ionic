import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController,
  IonicPage
} from "ionic-angular";
import { AddressPage } from "../address/address";
import { HomePage } from '../home/home';
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";

@IonicPage()
@Component({
  selector: "page-cart",
  templateUrl: "cart.html"
})
export class CartPage {

  public showtxt:boolean;
  public showcartdata:boolean;
  public baseUrlImage:String;

  cartquantity = 1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastController: ToastController,
    private auth: HkApiproviderProvider
  ) {
    this.baseUrlImage = auth.getimage();

    if(HomePage.cartData.length == 0){
      this.showtxt = true;
      this.showcartdata = false;
    }else{
      this.showtxt = false;
      this.showcartdata = true;
    }

    const data = JSON.parse(localStorage.getItem('userData'));
    
    let postData = {
      token: "",
      user_id:"",
      name:"",
      item_id:"",
      item_price:"",
    };
    postData.user_id = data.userData.user_id;
    postData.token = data.userData.token;
   
    HomePage.postData = postData;
    HomePage.authObj = auth;
    HomePage.shipping = auth.getshippingChargers();
    HomePage.calculatecart();
  }

  getCart() {
    return HomePage.cartData;
  }

  getCartLength() {
    return HomePage.cartData.length;
  }

  goToCheckout() {
    this.navCtrl.push(AddressPage);
  }

  stateChange(event, i) {
    HomePage.cartData[i].acceptchange = event.value === true ? "1" : "0";
  }

  chackAcceptChange(i) {
    return HomePage.cartData[i].acceptchange === "1";
  }

  changeQty(i, change){

    HomePage.changeQtyCart( HomePage.cartData[i].itemid, 
                            HomePage.cartData[i].itemname, 
                            HomePage.cartData[i].itemquantity, 
                            HomePage.cartData[i].itemquantitytype, 
                            HomePage.cartData[i].itemprice, 
                            HomePage.cartData[i].itemImage, 
                            change);

      HomePage.calculatecart();
    }

  removeItem(i){
    HomePage.removeItem(i);
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getFinalAmountLocal(){
    return HomePage.finalamount;
  }

  getShipValue() {
    return HomePage.shipValue;
  }

}

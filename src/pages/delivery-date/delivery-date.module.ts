import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeliveryDatePage } from './delivery-date';

@NgModule({
  declarations: [
    DeliveryDatePage,
  ],
  imports: [
    IonicPageModule.forChild(DeliveryDatePage),
  ],
})
export class DeliveryDatePageModule {}

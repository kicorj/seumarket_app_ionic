import { PaymentPage } from './../payment/payment';
import { Component } from '@angular/core';
import {   NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController } from 'ionic-angular';
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { HomePage } from '../home/home';
import { Order } from '../../shared/models/order.interface';

@Component({
  selector: 'page-delivery-date',
  templateUrl: 'delivery-date.html',
})
export class DeliveryDatePage {

  responseData :any;
  orderdataSet:any;
  deliveryDateData:any;
  neighborhoodsData:any;

  public userDetails:any;

  postData = {
    token: "",
    user_id:""
  };

  order = {} as Order;

  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    public auth: HkApiproviderProvider,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController) {

    const data = JSON.parse(localStorage.getItem('userData'));
    this.postData.user_id = data.userData.user_id;
    this.postData.token = data.userData.token;

    this.order = navParams.get('orderData');
    
    this.getdeliverydates();
  }

  getdeliverydates() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    this.auth.postData(this.postData, "getdeliverydates").then(
      result => {
        this.responseData = result;
        this.deliveryDateData = this.responseData.DeliveryDateData;
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryDatePage');
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getFinalAmountLocal(){
    return HomePage.finalamount;
  }

  getShipValue() {
    return HomePage.shipValue;
  }

  onSubmit() {
    if(this.order.dataentrega && this.order.recurrence) {
      this.navCtrl.push(PaymentPage, {'orderData': this.order});
    }
    else {
      let alert = this.alertCtrl.create( {
        title: 'Aviso',
        subTitle: 'Por favor preencha todos os campos',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}

import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { Http } from "@angular/http";
import { HomePage } from '../home/home';

@Component({
  selector: "page-edituser",
  templateUrl: "edituser.html"
})
export class EditUserPage {

  resposeData :any;
  orderdataSet:any;
  deliveryDateData:any;
  neighborhoodsData:any;

  public userDetails:any;

  postData = {
    token: "",
    user_id:""
  };

  userData = {
    token: "",
    user_id: "",
    fname: "",
    address: "",
    mobile: "",
    email:"",
    dataentrega: "",
    neighborhood_id: "",
    portaria: "",
    aceitasubstituicao: "",
  };

  edituser = {
    token: "",
    user_id: "",
    address: "",
    mobile: "",
    neighborhood_id: "",
    portaria: "",
  }

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public auth: HkApiproviderProvider,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public http: Http
  ) {

    const data = JSON.parse(localStorage.getItem('userData'));
    this.userData = data.userData;
   
    this.postData.user_id = this.userData.user_id;
    this.postData.token = this.userData.token;
    this.getneighborhoods();
    this.getuserdata();
  }

  getuserdata() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    this.auth.postData(this.postData, "getuserdata").then(
      result => {
        this.resposeData = result;
        this.userData = this.resposeData.userData;
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }


  getneighborhoods() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    this.auth.postData(this.postData, "getneighborhoods").then(
      result => {
        this.resposeData = result;
        this.neighborhoodsData = this.resposeData.NeighborhoodsData;
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  checkLobby() {
    return this.userData.portaria === "1";
  }

  stateChangeLobby(event) {
    this.userData.portaria = event.value === true ? "1" : "0";
  }

  helpLobby() {
    let alert = this.alertCtrl.create({
      title: 'Deixar na portaria',
      subTitle: '<br/>Indique se você prefere que nosso entregador deixe as compras na portaria do seu prédio ou condomínio.',
      buttons: ['OK']
    });
    alert.present();
  }

  onSubmit(){

    let zest = this.loadingCtrl.create({
      content: "Atualizando dados cadastrais..",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();
    
    this.edituser.token = this.userData.token;
    this.edituser.user_id = this.userData.user_id;
    this.edituser.mobile = this.userData.mobile;
    this.edituser.address = this.userData.address === null ? "" : this.userData.address;
    this.edituser.neighborhood_id = this.userData.neighborhood_id;
    this.edituser.portaria = this.userData.portaria;

    if(this.edituser.address != "" && this.edituser.mobile != "" && this.edituser.neighborhood_id && this.edituser.user_id != "" && this.edituser.token != ""){
      this.auth.postData(this.edituser, "edituser").then(
        result => {
          zest.dismiss();
          this.resposeData = result;
          localStorage.setItem('userData', JSON.stringify(this.resposeData) );
          this.navCtrl.setRoot(HomePage);
        },
        err => {
          zest.dismiss();
        }
      );
      }
      else{
          let alert = this.alertCtrl.create({
            title: 'Aviso',
            subTitle: 'Por favor preencha todos os campos',
            buttons: ['OK']
          });
          alert.present();
          zest.dismiss();
      }
    }
  }


import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { RegisterPage} from '../register/register'; 


@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  resposeData : any;
  userData = {"email":""};

  constructor(public navCtrl: NavController, public navParams: NavParams, private menuCtrl: MenuController, public authService: HkApiproviderProvider, public alertCtrl: AlertController, private loadingCtrl: LoadingController) {
      this.menuCtrl.enable(false);
  }


  login(){
    let loader = this.loadingCtrl.create({
          content: "Carregando...",
          duration: 10000
        });  
        loader.present();
    if(this.userData.email != ""){
     this.authService.postData(this.userData, "forgotpassword").then((result) =>{
      loader.dismiss();
     this.resposeData = result;
     if(this.resposeData.userData){
            this.showalertsuccess();
            this.navCtrl.setRoot(LoginPage);
        }else if(this.resposeData.error){
          this.showalertinfo();
        } 
     }, (err) => {
     loader.dismiss();
      this.showalertinfo();
     });
    }
    else{
     loader.dismiss();
     this.showalertinfo();
    }
   
   }

   showalertsuccess(){
    let alert = this.alertCtrl.create({
      title:"Alerta",
      subTitle:"As instruções para a redefinição de senha foram enviadas para o seu email.",
      buttons:["OK"]
    });
    alert.present();
  }

  showalertinfo(){
    let alert = this.alertCtrl.create({
      title:"Alerta",
      subTitle:"Dados inválidos",
      buttons:["OK"]
    });
    alert.present();
  }

  onregister(){
    this.navCtrl.setRoot(RegisterPage);
  }

}

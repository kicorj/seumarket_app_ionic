import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, LoadingController,ToastController, MenuController } from "ionic-angular";
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { ProductDescPage } from "../product-desc/product-desc";
import { ProductSearchPage } from "../product-search/product-search";
import { OffersPage } from '../offers/offers';
import { CartPage } from "../cart/cart";
import { ProductsPage } from "../products/products";
import { ProductBannerPage } from "../product-banner/product-banner";
import { CartItem } from "../../shared/models/cartitem.interface";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {
  public responseData: any;
  public userDetails:any;

  public static cartData: CartItem[];
  public static postData = {
    token: "",
    user_id:"",
    name:"",
    item_id:"",
    item_price:"",
  };

  public static authObj: HkApiproviderProvider;

  cartCounter  = 0;

  public static shipping = [];
  public static shipValue = 0;
  public static amount = 0;
  public static finalamount = 0;

  public baseUrlImage:String;
  public baseUrlBanner:String;

  public homeproductdataSet: any;
  public categoryFullData: any;
  public bannerFullData: any;

  public productFullData: any = [];
  moreProducts: any[];
  page: number;
  searchQuery: string = "";
  categoryList: any = "";
  constructor(
    public navCtrl: NavController,
    private auth: HkApiproviderProvider,
    public navPram: NavParams,
    public toastCtrl : ToastController,
    public loadingCtrl : LoadingController,
    public menuCtrl : MenuController,
  ) {
    
  }

  ngOnInit(): void {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    this.menuCtrl.enable(true);
    this.baseUrlImage = this.auth.getimage();
    this.baseUrlBanner = this.auth.getbanner();
    this.homepageproducts();

    HomePage.postData.user_id = data.userData.user_id;
    HomePage.postData.token = data.userData.token;
    HomePage.authObj = this.auth;

    HomePage.shipping = this.auth.getshippingChargers();
    HomePage.calculatecart();
    this.refreshcartOnServer();
    this.checkServerInitializeParameters();
  }

  itemSelected(category) {
    this.navCtrl.push(ProductsPage, { category: category });
  }

  gotocart(){
    this.navCtrl.push(CartPage);
  }

  checkServerInitializeParameters() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    this.auth.postData(HomePage.postData, "getuserdata").then(
      result => {
        const responseData:any = result;
        if (responseData.userData) {
          const userData = responseData.userData;
          if(userData.reloadCartOnDevice == 1) {
            HomePage.clearcart();
            HomePage.authObj.clearReloadCartOnDevice(HomePage.postData);
          } 
        }

        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  homepageproducts() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  
    zest.present();

    this.auth.postData(this.userDetails, "gethomepage").then(
      result => {
        this.responseData = result;
        this.homeproductdataSet = this.responseData.HomeData;
        this.categoryFullData = this.responseData.CateData;
        this.bannerFullData = this.responseData.BannData;
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  openProductDetailPage(
    itemId: String,
    itemName: String,
    itemImage: String,
    itemDesc: String,
    itemQuantity: String,
    itemQuantityType: String,
    itemPrice: String,
    itemCategoryId: String,    
    itemCategoryName: String,
    itemSubcategoryId: String,
    itemSubcategoryName: String,
  ) {
    this.navCtrl.push(ProductDescPage, {
      itemId: itemId,
      itemName: itemName,
      itemDesc: itemDesc,
      itemQuantity: itemQuantity,
      itemQuantityType: itemQuantityType,
      itemPrice: itemPrice,
      itemImage: itemImage,
      itemCategoryId: itemCategoryId,
      itemCategoryName: itemCategoryName,
      itemSubcategoryId: itemSubcategoryId,
      itemSubcategoryName: itemSubcategoryName
    });
  }

  refreshcartOnServer() {
    this.auth.refreshCart(this.userDetails);
  }


  public static addToCart(itemid:any, itemname:any, itemquantity:any, itemquantitytype:any, itemprice:any, itemImage:any) {
    HomePage.storeToCart(itemid,itemname,itemquantity,itemquantitytype,itemprice, itemImage, 1);
    HomePage.flushCartData();
  }

  public static calculateShipment(amount){
    if (this.shipping && this.shipping.length > 0) {
      this.shipValue = parseFloat(amount) < parseFloat(this.shipping[0]) ? this.shipping[1] : 0;
    }
  }

  public static calculatecart(){
    this.amount = 0;

    if (HomePage.cartData) {
      for(let item of HomePage.cartData){
        this.amount += parseFloat(item.itemprice) * item.Mquantity; 
      }
    }

    if (this.amount === 0) {    
      HomePage.finalamount = 0; 
    } else {
      this.calculateShipment(this.amount);
      HomePage.finalamount = +HomePage.amount + +HomePage.shipValue;  
    }

  }

  public static removeItem(i){
    if (i > -1) {
      HomePage.cartData.splice(i, 1);
      this.calculatecart();
    }
    HomePage.flushCartData();
  }

  public static storeToCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change) {
    HomePage.changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change);
  }

  public static qtyOnCart(itemid) {
    const resultIndex = HomePage.cartData ? HomePage.cartData.findIndex( product => product.itemid === itemid ) : -1;
    return resultIndex < 0 ? 0 : HomePage.cartData[resultIndex].Mquantity;
  }

  public static changeQtyCart(itemid: string, itemname: string, itemquantity: string, itemquantitytype: string, itemprice: string, itemImage: string, change: number) {

    const MquantityOne:number = 1;
    var itemtotal = itemprice;

    this.postData.item_id = itemid;
    this.postData.name = itemname;
    this.postData.item_price = itemprice;

    const resultIndex = HomePage.cartData ? HomePage.cartData.findIndex( product => product.itemid === itemid ) : -1;

    if (change === 1) {
      if (resultIndex >= 0) {
        HomePage.cartData[resultIndex].Mquantity++;
      } else {

        if (!HomePage.cartData) {
          HomePage.cartData = [];
        }

        HomePage.cartData.push({
          itemid,
          itemname,
          itemquantity,
          itemquantitytype,
          itemprice,
          itemtotal,
          itemImage,
          Mquantity: MquantityOne,
          acceptchange: change.toString(),
        });
      }      
      this.authObj.sumItemCart(this.postData);
    } else {
      if (resultIndex >= 0) {
        HomePage.cartData[resultIndex].Mquantity--;
        this.authObj.subtractItemCart(this.postData);
        if (HomePage.cartData[resultIndex].Mquantity < 1) {
          HomePage.removeItem(resultIndex);
          this.authObj.removeItemCart(this.postData);
        }
      }
    }
    HomePage.calculatecart();
  }

  public static clearcart(){    
    HomePage.cartData = new Array<CartItem>();
    HomePage.amount = 0;
    localStorage.setItem('cartData', JSON.stringify(HomePage.cartData) );    
  }

  public static flushCartData() {
    localStorage.setItem('cartData', JSON.stringify(HomePage.cartData) );
  }

  gotoSearch(){
    this.navCtrl.push(ProductSearchPage);
  }

  gotoBanner(banner: any){
    if (banner.text)
      this.navCtrl.push(ProductBannerPage, {banner: JSON.stringify(banner)});
  }

  gotoproducts(category_id: string, category_name: string) {
    this.navCtrl.push(ProductsPage, {
      category_id: category_id,
      category_name: category_name
    });
  }

  gotoOffer(){
    this.navCtrl.push(OffersPage);
  }

  addToCart(itemid:any, itemname:any, itemquantity:any, itemquantitytype:any, itemprice:any, itemImage:any) {
    HomePage.addToCart(itemid,itemname,itemquantity,itemquantitytype,itemprice, itemImage);
  }

  changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change) {
    HomePage.changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change);
  }

  removeItem(i){
    console;
    HomePage.removeItem(i);
  }

  getCartCount(){
    var total : number = 0;

    if (HomePage.cartData) {
      for (var i = 0; i < HomePage.cartData.length; i++) {
        total += HomePage.cartData[i].Mquantity;
      }
    }

    return total;
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getFinalAmountLocal(){
    return HomePage.finalamount;
  }

  qtyOnCart(itemid) {
    return HomePage.qtyOnCart(itemid);
  }
}

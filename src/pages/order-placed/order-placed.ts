import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";

@IonicPage()
@Component({
  selector: 'page-order-placed',
  templateUrl: 'order-placed.html',
})
export class OrderPlacedPage {

  public userDetails:any;

  postData = {
    token: "",
    user_id:"",
    orderid: ""
  };

  viewData={
    orderid : "",
    fname : "",
    lname : "",
    area : "",
    address : "",
    mobile : "",
    ordertime : "",
    dataentrega: "",
    amount: 0,
    shipValue:0,
    finalamount: 0,
    paymentform:"",
  };
  resposeData:any;
  orderDetail:any;

  shipping = [];
  shipValue = 0;
  amount = 0;
  finalamount = 0;

  paymentformLabel = "";

  constructor(private loadingCtrl: LoadingController, private auth: HkApiproviderProvider, public navCtrl: NavController, public navParams: NavParams) {

      const data = JSON.parse(localStorage.getItem('userData'));
      this.userDetails = data.userData;

      this.postData.user_id = this.userDetails.user_id;
      this.postData.token = this.userDetails.token;

      // console.log('NavParams');
      // console.log(navParams);

      this.postData.orderid = navParams.get("orderid");
      this.viewData.orderid = navParams.get("orderid");
      this.viewData.fname = navParams.get("fname");
      this.viewData.lname = navParams.get("lname");
      this.viewData.area = navParams.get("area");
      this.viewData.address = navParams.get("address");
      this.viewData.mobile = navParams.get("mobile");
      this.viewData.ordertime = navParams.get("ordertime");
      this.viewData.dataentrega = navParams.get("dataentrega");
      this.viewData.paymentform = navParams.get("paymentform");

      this.paymentformLabel = this.viewData.paymentform === "2" ? "Cartão de crédito" : "Transferência Bancária";

      console.log(this.viewData.paymentform);
      console.log(this.paymentformLabel);
      
      this.getOrderDetail();

      this.shipping = auth.getshippingChargers();      
  }

  getOrderDetail(){
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  
  
    zest.present();
    this.auth.postData(this.postData, "fetchorderdetail").then(
      result => {
        this.resposeData = result;
        this.orderDetail = this.resposeData.OrderDetail;
        this.calculatetotal();
        this.viewData.amount = this.amount;
        this.viewData.shipValue = this.shipValue;
        this.viewData.finalamount = this.finalamount;
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    )
    
  }

    calculateShipment(amount){

      if (this.shipping && this.shipping.length > 0) {
        // console.log("entrou");
        this.shipValue = parseFloat(amount) < parseFloat(this.shipping[0]) ? this.shipping[1] : 0;
      }
    }

    calculatetotal(){
      this.amount = 0;
      for(let item of this.orderDetail){
        this.amount += parseFloat(item.itemprice) * parseInt(item.Mquantity); 
        //console.log(this.amount);
    }
  
    this.calculateShipment(this.amount);
  
    this.finalamount = +this.amount + +this.shipValue;

    // console.log(this.shipValue);
    // console.log(this.amount);
    // console.log(this.finalamount);
  }

}

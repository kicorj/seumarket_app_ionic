import { Component } from '@angular/core';
import { IonicPage, NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController } from 'ionic-angular';
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { HomePage } from '../home/home';
import { OrderConfirmPage } from '../order-confirm/order-confirm';
import { Order } from '../../shared/models/order.interface';

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  responseData :any;
  orderdataSet:any;
  deliveryDateData:any;
  neighborhoodsData:any;

  public userDetails:any;

  postData = {
    token: "",
    user_id:""
  };

  order = {} as Order;
  couponApplied = false;
  discount = 1;
  
  brand_image = "";
  loaded = false;

  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    public auth: HkApiproviderProvider,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController) {

    const data = JSON.parse(localStorage.getItem('userData'));
    this.postData.user_id = data.userData.user_id;
    this.postData.token = data.userData.token;
    
    this.order = navParams.get('orderData');
  }

  cardFunction(value) {
    var re = {
      electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
      maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
      dankort: /^(5019)\d+$/,
      interpayment: /^(636)\d+$/,
      unionpay: /^(62|88)\d+$/,
      visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
      mastercard: /^5[1-5][0-9]{14}$/,
      amex: /^3[47][0-9]{13}$/,
      diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
      discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
      jcb: /^(?:2131|1800|35\d{3})\d{11}$/
  }

  for(var key in re) {
      if(re[key].test(value)) {
          return key;
      }
  }
  }

  detectCardType(event) {
    const value = event.value.replace(/\s/g, '');
    const brand = this.cardFunction(value);
    this.order.brand = brand ? brand : "";
    this.brand_image = brand ? `././assets/imgs/${brand}.png` : "";
    // console.log(this.order.brand);
    // console.log(this.brand_image);
}

  holderNameToUppercase(value:string) {
    this.order.holder_name = value.toUpperCase();
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getDiscount(){
    return this.discount == 1 ? 0 : HomePage.amount * this.discount;
  }
  
  getFinalAmountLocal(){
    return HomePage.finalamount - this.getDiscount();
  }

  getShipValue() {
    return HomePage.shipValue;
  }

  isCreditCard() {
    return this.order.paymentform === '2';
  }

  getCouponData() {
    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    let postDataCoupon = {
      token: this.postData.token,
      user_id: this.postData.user_id,
      coupon_code: this.order.coupon_code
    };

    this.auth.postData(postDataCoupon, "validateCoupon").then(
      result => {
        this.responseData = result;
          const couponData = this.responseData.CouponData;
          if (couponData) {
            this.couponApplied = true;
            this.discount = couponData.discount/100;
          } else {
            const msgError = this.responseData.error.text ? this.responseData.error.text : 'Erro ao validar Cupom.';
            this.order.coupon_code = '';
            let alert = this.alertCtrl.create( {
              title: 'Aviso',
              subTitle: msgError,
              buttons: ['OK']
            });
            alert.present();
          }
        
        zest.dismiss();
      },
      err => {
        zest.dismiss();
        this.couponApplied = false;
        this.order.coupon_code = '';
        this.discount = 1;
        let alert = this.alertCtrl.create( {
          title: 'Aviso',
          subTitle: 'Erro ao validar Cupom.',
          buttons: ['OK']
        });
        alert.present();        
      }
    );
  }

  onSubmit() {
    
    let zest = this.loadingCtrl.create({
      content: "Gerando o Pedido..",
      duration:20000,
      spinner:'crescent'
    });  

    zest.present();

    let formOk = false;

    formOk = ((this.order.cpfcnpj != "" && this.order.cpfcnpj.length == 14) &&
              (this.order.paymentform === '1') ||
              (this.order.paymentform === '2' && 
                  this.order.holder_name != "" && 
                  this.order.card_number != "" && 
                  this.order.expiration_date != "" && 
                  this.order.cvv != "" &&
                  this.brand_image != ""
                  )
                );


    if(formOk) {
      this.order.discount = this.getDiscount();
      this.order.items = HomePage.cartData;
      // console.log(JSON.stringify(this.order));
      this.auth.postData(this.order, "placeorder").then(
              result => {
                zest.dismiss();
                this.responseData = result;
                if(this.responseData.success){
                  this.orderdataSet = this.responseData.success;
                  this.navCtrl.setRoot(OrderConfirmPage,{ OrderId: this.orderdataSet });
                }else{
                  // console.log(this.responseData);
                }
              },
              err => {
                zest.dismiss();
              }
            );
    }
    else {
      let alert = this.alertCtrl.create( {
        title: 'Aviso',
        subTitle: 'Por favor preencha todos os campos corretamente.',
        buttons: ['OK']
      });
      alert.present();
      zest.dismiss();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

}

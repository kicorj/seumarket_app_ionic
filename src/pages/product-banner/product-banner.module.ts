import { ProductBannerPage } from './product-banner';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    ProductBannerPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductBannerPage),
  ],
})
export class ProductSearchPageModule {}

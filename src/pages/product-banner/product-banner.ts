import { Component, OnInit } from "@angular/core";
import {
  LoadingController,
  NavController,
  ModalController,
  AlertController,
  ToastController,
  NavParams
} from "ionic-angular";
import { CartPage } from "../cart/cart";
import { ProductDescPage } from "../product-desc/product-desc";
import { HomePage } from "../home/home";
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";

@Component({
  selector: "page-product-banner",
  templateUrl: "product-banner.html"
})
export class ProductBannerPage implements OnInit {

  public resposeData: any;
  public userDetails: any;

  postDataSearch = {
    token: "",
    user_id:"",
    product_ids: ""
  };
  public bannerProductsData: any = [];
  public baseUrlImage:String;

  bannerData = {
    id: "",
    title: "",
    text: "",
    product_ids:"",
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public auth: HkApiproviderProvider,
    public toastController: ToastController,
    public loadingCtrl: LoadingController
  ) {
    
  }


  ngOnInit(): void {
    this.baseUrlImage = this.auth.getimage();
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;

    this.postDataSearch.user_id = this.userDetails.user_id;
    this.postDataSearch.token = this.userDetails.token;

    let postData = {
      token: "",
      user_id:"",
      name:"",
      item_id:"",
      item_price:"",
    };
    postData.user_id = data.userData.user_id;
    postData.token = data.userData.token;
   
    HomePage.postData = postData;
    HomePage.authObj = this.auth;        
    HomePage.shipping = this.auth.getshippingChargers();
    HomePage.calculatecart();

    this.bannerData = JSON.parse(this.navParams.get("banner"));

    this.searchProduct();
  }

  openCartPage() {
    this.navCtrl.push(CartPage);
  }

  public searchProduct(){

    this.postDataSearch.product_ids = this.bannerData.product_ids;

    let zest = this.loadingCtrl.create({
      content: "Carregando...",
      duration: 20000,
      spinner: "crescent"
    });

    zest.present();

    this.auth.postData(this.postDataSearch, "searchproductbyids").then(
      result => {
        this.resposeData = result;
        this.bannerProductsData = this.resposeData.SearchData;
        zest.dismiss();
      },
      err => {
        zest.dismiss();
      }
    );
  }

  openProductDetailPage(
    itemId: String,
    itemName: String,
    itemImage: String,
    itemDesc: String,
    itemQuantity: String,
    itemQuantityType: String,
    itemPrice: String,
    itemCategoryId: String,    
    itemCategoryName: String,
    itemSubcategoryId: String,
    itemSubcategoryName: String,
  ) {
    this.navCtrl.push(ProductDescPage, {
      itemId: itemId,
      itemName: itemName,
      itemDesc: itemDesc,
      itemQuantity: itemQuantity,
      itemQuantityType: itemQuantityType,
      itemPrice: itemPrice,
      itemImage: itemImage,
      itemCategoryId: itemCategoryId,
      itemCategoryName: itemCategoryName,
      itemSubcategoryId: itemSubcategoryId,
      itemSubcategoryName: itemSubcategoryName
    });
  }


  gotocart(){
    this.navCtrl.push(CartPage);
  }

  addToCart(itemid:any, itemname:any, itemquantity:any, itemquantitytype:any, itemprice:any, itemImage:any) {
    HomePage.addToCart(itemid,itemname,itemquantity,itemquantitytype,itemprice, itemImage);
  }

  changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change) {
    HomePage.changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change);
  }

  removeItem(i){
    HomePage.removeItem(i);
  }

  getCartCount(){
    var total : number = 0;

    for (var i = 0; i < HomePage.cartData.length; i++) {
      total += HomePage.cartData[i].Mquantity;
    }

    return total;
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getFinalAmountLocal(){
    return HomePage.finalamount;
  }

  qtyOnCart(itemid) {
    return HomePage.qtyOnCart(itemid);
  }
}

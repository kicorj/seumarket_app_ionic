import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { CartPage } from "../cart/cart";
import { HomePage } from "../home/home";
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { ProductsPage } from "../products/products";
import { ProductSearchPage } from "../product-search/product-search";

@Component({
  selector: "page-product-desc",
  templateUrl: "product-desc.html"
})
export class ProductDescPage {
  itemId: string;
  itemName: string;
  itemDesc: String;
  itemQuantity: string;
  itemQuantityType: string;
  itemPrice: string;
  itemImage: string;
  itemCategoryId: string;
  itemCategoryName: string;
  itemSubcategoryId: string;
  itemSubcategoryName: string;

  public baseUrlImage: String;

  // public postData = {
  //   token: "",
  //   user_id:""
  // };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastController: ToastController,
    private auth: HkApiproviderProvider
  ) {
    
    this.baseUrlImage = auth.getimageMedium();

    this.itemId = navParams.get("itemId");
    this.itemName = navParams.get("itemName");
    this.itemDesc = navParams.get("itemDesc");
    this.itemQuantity = navParams.get("itemQuantity");
    this.itemQuantityType = navParams.get("itemQuantityType");
    this.itemPrice = navParams.get("itemPrice");
    this.itemImage = navParams.get("itemImage");
    this.itemCategoryId = navParams.get("itemCategoryId");
    this.itemCategoryName = navParams.get("itemCategoryName");
    this.itemSubcategoryId = navParams.get("itemSubcategoryId");
    this.itemSubcategoryName = navParams.get("itemSubcategoryName");

    const data = JSON.parse(localStorage.getItem('userData'));
    
    let postData = {
      token: "",
      user_id:"",
      name:"",
      item_id:"",
      item_price:"",
    };
    postData.user_id = data.userData.user_id;
    postData.token = data.userData.token;
   
    HomePage.postData = postData;
    HomePage.authObj = auth;
    HomePage.shipping = auth.getshippingChargers();
    HomePage.calculatecart();
  }

  ionViewDidLoad() {}

  openCartPage() {
    this.navCtrl.push(CartPage);
  }

  gotocart(){
    this.navCtrl.push(CartPage);
  }

  gotoSearch(){
    this.navCtrl.push(ProductSearchPage);
  }
  
  gotoproducts(category_id: string, category_name: string) {
    this.navCtrl.push(ProductsPage, {
      category_id: category_id,
      category_name: category_name
    });
  }

  addToCart(itemid:any, itemname:any, itemquantity:any, itemquantitytype:any, itemprice:any, itemImage:any) {
    HomePage.addToCart(itemid,itemname,itemquantity,itemquantitytype,itemprice, itemImage);
  }

  changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change) {
    HomePage.changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change);
  }

  removeItem(i){
    HomePage.removeItem(i);
  }

  getCartCount(){
    var total : number = 0;

    for (var i = 0; i < HomePage.cartData.length; i++) {
      total += HomePage.cartData[i].Mquantity;
    }

    return total;
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getFinalAmountLocal(){
    return HomePage.finalamount;
  }

  qtyOnCart(itemid) {
    return HomePage.qtyOnCart(itemid);
  }
}

import { Component } from '@angular/core';
import { LoadingController, NavController, ModalController, AlertController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import { ProductDescPage } from '../product-desc/product-desc';
import { HomePage } from '../home/home';
import { HkApiproviderProvider } from '../../providers/hk-apiprovider/hk-apiprovider';
import { CartPage } from "../cart/cart";
import { ProductSearchPage } from '../product-search/product-search';


@Component({
  templateUrl: 'products.html',
})

export class ProductsPage {
 
  public resposeData : any;
  public userDetails: any;
  postData={
    "user_id":"",
    "token":"",
    "categoryname":"",
    "category_id":"",
    "subcategory_id":"",
  }

  public baseUrlImage:String;
  category_id :string;
  category_name :string;
  
  public dataSetitems : any;
  public categoryFullData : any = [];
  public subcategoryFullData : any = [];

  products:any[];
  category:any="";
  iteminfo:any="";
  itemdatafull:any="";
  itemFullArray:any=[];

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public auth:HkApiproviderProvider,
    public toastController:ToastController,
    public loadingCtrl : LoadingController
	) {
    this.baseUrlImage = auth.getimage();
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    
    this.category_name = navParams.get('category_name');
    this.category_id = navParams.get('category_id');

    this.postData.user_id = this.userDetails.user_id;
    this.postData.token = this.userDetails.token;
    this.postData.category_id = this.category_id;
    this.postData.categoryname = this.category_name;
    
    this.getcategorydata();
    this.getsubcategorydata();
    
    let postData = {
      token: "",
      user_id:"",
      name:"",
      item_id:"",
      item_price:"",
    };
    postData.user_id = this.userDetails.user_id;
    postData.token = data.userData.token;
   
    HomePage.postData = postData;
    HomePage.authObj = auth;

    HomePage.shipping = auth.getshippingChargers();
    HomePage.calculatecart();
	}

  loadByProductsBySubCategory(categoryid:any, subcategory: any){
    this.postData.subcategory_id = subcategory;
    this.getcategorydata();
  }

  public getcategorydata() {
  let zest = this.loadingCtrl.create({
    content: "Carregando...",
    duration:20000,
    spinner:'crescent'
  });  

  zest.present();

    this.auth.postData(this.postData, "categoryfilter").then((result) => {
        this.resposeData = result;
        this.categoryFullData = this.resposeData.CateFilterData;
        zest.dismiss();
    }, (err) => {
      zest.dismiss();
    });
  }

  public getsubcategorydata() {
  let zest = this.loadingCtrl.create({
    content: "Carregando...",
    duration:20000,
    spinner:'crescent'
  });  

  zest.present();

    this.auth.postData(this.postData, "subcategoryfilter").then((result) => {
        this.resposeData = result;
        this.subcategoryFullData = this.resposeData.SubCateFilterData;
        zest.dismiss();
    }, (err) => {
      zest.dismiss();
    });
  }

  openProductDetailPage(
    itemId: String,
    itemName: String,
    itemImage: String,
    itemDesc: String,
    itemQuantity: String,
    itemQuantityType: String,
    itemPrice: String,
    itemCategoryId: String,    
    itemCategoryName: String,
    itemSubcategoryId: String,
    itemSubcategoryName: String,
  ) {
    this.navCtrl.push(ProductDescPage, {
      itemId: itemId,
      itemName: itemName,
      itemDesc: itemDesc,
      itemQuantity: itemQuantity,
      itemQuantityType: itemQuantityType,
      itemPrice: itemPrice,
      itemImage: itemImage,
      itemCategoryId: itemCategoryId,
      itemCategoryName: itemCategoryName,
      itemSubcategoryId: itemSubcategoryId,
      itemSubcategoryName: itemSubcategoryName
    });
  }

  gotocart(){
    this.navCtrl.push(CartPage);
  }

  gotoSearch(){
    this.navCtrl.push(ProductSearchPage);
  }

  addToCart(itemid:any, itemname:any, itemquantity:any, itemquantitytype:any, itemprice:any, itemImage:any) {
    HomePage.addToCart(itemid,itemname,itemquantity,itemquantitytype,itemprice, itemImage);
  }

  changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change) {
    HomePage.changeQtyCart(itemid, itemname, itemquantity, itemquantitytype, itemprice, itemImage, change);
  }

  removeItem(i){
    HomePage.removeItem(i);
  }

  getCartCount(){
    var total : number = 0;

    for (var i = 0; i < HomePage.cartData.length; i++) {
      total += HomePage.cartData[i].Mquantity;
    }

    return total;
  }

  getAmountLocal(){
    return HomePage.amount;
  }

  getFinalAmountLocal(){
    return HomePage.finalamount;
  }

  qtyOnCart(itemid) {
    return HomePage.qtyOnCart(itemid);
  }
}

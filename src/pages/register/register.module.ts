import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import {NgxMaskIonicModule} from 'ngx-mask-ionic'

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    NgxMaskIonicModule,
  ],
})
export class RegisterPageModule {}

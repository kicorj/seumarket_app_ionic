import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { HkApiproviderProvider } from "../../providers/hk-apiprovider/hk-apiprovider";
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  resposeData : any;
  userData = {"name":"","mobile":"","email":"", "password":""};

  constructor(public navCtrl: NavController, public navParams: NavParams, private menuCtrl: MenuController, public authService: HkApiproviderProvider, public alertCtrl: AlertController, private loadingCtrl: LoadingController) {
      this.menuCtrl.enable(false);
  }


 register(){
    let loader = this.loadingCtrl.create({
          content: "Carregando...",
          duration: 10000
        });  
        loader.present();
    if(this.userData.email != "" && this.userData.password != "" && this.userData.name != "" && this.userData.mobile != ""){
     
      // console.log(this.userData);
      if (!this.validateEmail(this.userData.email)) {
        loader.dismiss();
        // console.log("email inválido!!");
        this.showInvalidEmail();
        return;
      }

      this.authService.postData(this.userData, "register").then((result) =>{
      loader.dismiss();
     this.resposeData = result;
     if(this.resposeData.userData){
            this.showsucessinfo();
            this.navCtrl.setRoot(LoginPage);
      }else if(this.resposeData.error){
          this.showalertinfo(this.resposeData.error);
      } 
     }, (err) => {
     loader.dismiss();
      this.showalertinfo(null);
       //Connection failed message
     });
    }
    else{
      // console.log(this.userData);
     loader.dismiss();
     this.showAlertRequiredFields();
    }
   
   }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  showAlertRequiredFields(){
    let alert = this.alertCtrl.create({
      title:"Aviso",
  subTitle:"Preencha todos os campos corretamente.",
      buttons:["OK"]
    });
    alert.present();
  }

  showInvalidEmail(){
    let alert = this.alertCtrl.create({
      title:"Aviso",
  subTitle:"Informe um email válido.",
      buttons:["OK"]
    });
    alert.present();
  }

  showalertinfo(error){

    let errorMessage = "Erro ao tentar processar cadastro.";
    let errorEmailMessage = "Email já cadastrado no Sistema.";

    if (error.text == errorEmailMessage) {
      errorMessage = errorEmailMessage;
    }

    let alert = this.alertCtrl.create({
      title:"Erro",
      subTitle: errorMessage,
      buttons:["OK"]
    });
    alert.present();
  }

  showsucessinfo(){
    let alert = this.alertCtrl.create({
      title:"Sucesso",
      subTitle:"Cadastro realizado com sucesso!",
      buttons:["OK"]
    });
    alert.present();
  }

  onlogin(){
    this.navCtrl.setRoot(LoginPage);
  }

}

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http,Headers } from '@angular/http';
import { Platform } from 'ionic-angular';

@Injectable()
export class HkApiproviderProvider {

  baseUrl = 'https://app.seumarket.com/';
  // baseUrl = 'http://localhost:8003/';
  data: any;

  imagebaseurl = 'https://seumarket.nyc3.digitaloceanspaces.com/photos/small/'; //******* */
  imagebasePhotoMediumUrl = 'https://seumarket.nyc3.digitaloceanspaces.com/photos/medium/';
  bannerbaseurl = 'https://seumarket.nyc3.digitaloceanspaces.com/photos/banners/';

  // shipping = 10;
  shipping = [200,15];

  /*********************************************** */

  constructor(public http: Http,
              public platform: Platform,) {
  }

  
  postData(credentials, type){
    return new Promise((resolve, reject) =>{
      let headers = new Headers();
      this.http.post(this.baseUrl+type, JSON.stringify(credentials), {headers: headers}).
      subscribe(res =>{
        // console.log(res);
        resolve(res.json());
      }, (err) =>{
        reject(err);
      });
    });
  }

  refreshCart(userDetails){

    let os = '';
    this.platform.is('ios') ? os = 'ios' : this.platform.is('android') ? os = 'android' : os = 'other';
    userDetails.platform = os;

    return new Promise(() =>{
      let headers = new Headers();
      this.http.post(this.baseUrl+'refreshcart', JSON.stringify(userDetails), {headers: headers}).
      subscribe();
    });
  }

  sumItemCart(postData){

    return new Promise(() =>{
      let headers = new Headers();
      this.http.post(this.baseUrl+'sumitemcart', JSON.stringify(postData), {headers: headers}).
      subscribe();
    });
  }

  clearReloadCartOnDevice(postData){

    return new Promise(() =>{
      let headers = new Headers();
      this.http.post(this.baseUrl+'clearreloadcartondevice', JSON.stringify(postData), {headers: headers}).
      subscribe();
    });
  }

  subtractItemCart(postData){

    return new Promise(() =>{
      let headers = new Headers();
      this.http.post(this.baseUrl+'subtractitemcart', JSON.stringify(postData), {headers: headers}).
      subscribe();
    });
  }

  removeItemCart(postData){

    return new Promise(() =>{
      let headers = new Headers();
      this.http.post(this.baseUrl+'removeitemcart', JSON.stringify(postData), {headers: headers}).
      subscribe();
    });
  }

    getimage(){
    return this.imagebaseurl;
  }

  getimageMedium(){
    return this.imagebasePhotoMediumUrl;
  }

  getbanner(){
    return this.bannerbaseurl;
  }

  getshippingChargers(){
    return this.shipping;
  }
  
}

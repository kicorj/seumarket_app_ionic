export interface CartItem {
    itemid: string,
    itemname: string,
    itemquantity: string,
    itemquantitytype: string,
    itemprice: string,
    itemtotal: string,
    itemImage: string,
    Mquantity: number,
    acceptchange: string,
}
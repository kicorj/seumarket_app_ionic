export interface Product {
    itemId: String,
    itemName: String,
    itemImage: String,
    itemDesc: String,
    itemQuantity: String,
    itemQuantityType: String,
    itemPrice: String,
    itemCategoryId: String,    
    itemCategoryName: String,
    itemSubcategoryId: String,
    itemSubcategoryName: String,
}